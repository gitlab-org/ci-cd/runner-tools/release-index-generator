package release

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/ci-cd/runner-tools/release-index-generator/fs"
)

func TestScanWorkingDirectory(t *testing.T) {
	wd, err := os.Getwd()
	require.NoError(t, err)

	gen := &defaultGenerator{}
	gen.shouldSkipFileFn = func(s string) bool {
		return s == "shouldskip.1"
	}
	gen.directoryScanner = fs.NewDirectoryScanner(filepath.Join(wd, "../testdata"))
	require.NoError(t, gen.scanWorkingDirectory())

	var count int
	_ = gen.directoryScanner.Walk(func(f fs.FileEntry) error {
		count++
		assert.NotEqual(t, f.FileName, "shouldskip.1")
		return nil
	})

	assert.NotEqual(t, 0, count)
}

func TestDefaultShouldSkipFileFn(t *testing.T) {
	gen, ok := NewGenerator("", Info{}, nil).(*defaultGenerator)
	require.True(t, ok)

	assert.True(t, gen.shouldSkipFileFn(indexFileName))
	assert.True(t, gen.shouldSkipFileFn(checksumsFileName))
	assert.True(t, gen.shouldSkipFileFn(checksumsSignatureFileName))
	assert.False(t, gen.shouldSkipFileFn("random-file"))
}
