package fs

import (
	"crypto/sha256"
	"fmt"
	"hash"
	"io"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
)

type ScannerWalkFn func(f FileEntry) error
type ScannerFilterFn func(path string, fileInfo os.FileInfo) bool

type Scanner interface {
	Walk(fn ScannerWalkFn) error
	Scan(filter ScannerFilterFn) error
	AddFile(path string) error
}

type directoryScanner struct {
	entries []FileEntry

	hasher  hash.Hash
	workDir string
}

func NewDirectoryScanner(workDir string) Scanner {
	return &directoryScanner{
		workDir: workDir,
		hasher:  sha256.New(),
	}
}

func (fs *directoryScanner) Walk(fn ScannerWalkFn) error {
	for _, entry := range fs.entries {
		err := fn(entry)
		if err != nil {
			return fmt.Errorf("walking the files: %w", err)
		}
	}

	return nil
}

func (fs *directoryScanner) Scan(filter ScannerFilterFn) error {
	return filepath.Walk(fs.workDir, func(path string, fileInfo os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("entering the path %q: %w", path, err)
		}

		if fileInfo.IsDir() || !filter(path, fileInfo) {
			return nil
		}

		err = fs.createFileEntry(path, fileInfo)
		if err != nil {
			return fmt.Errorf("creating file entry for %q: %w", path, err)
		}

		return nil
	})
}

func (fs *directoryScanner) createFileEntry(path string, fileInfo os.FileInfo) error {
	log := logrus.WithField("filePath", path)
	log.Info("Creating file entry...")

	if fileInfo.IsDir() {
		return fmt.Errorf("%q is a directory", path)
	}

	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("opening the file %q: %w", path, err)
	}
	defer f.Close()

	fs.hasher.Reset()
	n, err := io.Copy(fs.hasher, f)
	if err != nil {
		return fmt.Errorf("copying %q content to the SHA256 hash calculator: %w", path, err)
	}

	relativePath, err := filepath.Rel(fs.workDir, path)
	if err != nil {
		return fmt.Errorf("computing the relative path for %q and %q: %w", fs.workDir, path, err)
	}

	entry := FileEntry{
		FileName:     fileInfo.Name(),
		FullPath:     path,
		RelativePath: relativePath,
		Checksum:     fmt.Sprintf("%x", fs.hasher.Sum(nil)),
		SizeMb:       float64(n) / 1024 * 1024,
	}

	fs.entries = append(fs.entries, entry)

	log.WithFields(logrus.Fields{
		"checksum": entry.Checksum,
		"size_mb":  entry.SizeMb,
	}).Info("File entry created")

	return nil
}

func (fs *directoryScanner) AddFile(path string) error {
	fileInfo, err := os.Stat(path)
	if err != nil {
		return fmt.Errorf("os.Stat on %q: %w", path, err)
	}

	return fs.createFileEntry(path, fileInfo)
}
