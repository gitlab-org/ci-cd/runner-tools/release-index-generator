package fs

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestDirectoryScanner_Scan(t *testing.T) {
	wd, err := os.Getwd()
	require.NoError(t, err)

	scanner := NewDirectoryScanner(filepath.Join(wd, "../testdata"))

	err = scanner.Scan(func(path string, fileInfo os.FileInfo) bool {
		return fileInfo.Name() != "test.1"
	})
	require.NoError(t, err)

	assert.NotEmpty(t, scanner.(*directoryScanner).entries)
	_ = scanner.Walk(func(f FileEntry) error {
		assert.NotEqual(t, f.FileName, "test.1")
		return nil
	})
}
