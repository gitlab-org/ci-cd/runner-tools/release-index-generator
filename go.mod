module gitlab.com/gitlab-org/ci-cd/runner-tools/release-index-generator

go 1.13

require (
	github.com/jstemmer/go-junit-report v0.9.1
	github.com/mitchellh/gox v1.0.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	gitlab.com/gitlab-org/ci-cd/runner-tools/gitlab-changelog v0.0.0-20200301232733-9a7758323015
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
)
