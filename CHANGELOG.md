## v0.5.0 (2021-03-22)

### Maintenance

- Update changelog generator configuration !23

### Other changes

- Log more details on fileEntry creation !24

## v0.4.1 (2020-07-08)

### Bug fixes

- Fix index generator not picking up build artifacts !22

## v0.4.0 (2020-06-26)

### Bug fixes

- Fix scripting of latest stable release uploading !16
- Fix scripting for GCS release 'latest' detection !15

### Maintenance

- Migrate from CodeClimate to golangci-lint !20

### Other changes

- Upgrade mockery to v1.1.0 !21
- Pin CI jobs to gitlab-org runners !18
- Rely on `git ls-files` and `git diff` for checking mocks !17

## v0.3.0 (2020-03-17)

### New features

- Improve logging !12

### Bug fixes

- Make steps ordering static !14

### Maintenance

- Move the release stage jobs off the custom runner !13
- Move mocks !10
- Improve errors wording !9

## v0.2.0 (2020-03-06)

### Technical debt reduction

- Update changelog generation scripting !8
- Fix environment URLs !7

## v0.1.0 (2020-03-05)

### Technical debt reduction

- Add changelog generation scripting !6
- Publish releases in GCS !5
- Create one place where CI environment versions are defined !4
- Use html/template !2

### Other changes

- Initial implementation !1

